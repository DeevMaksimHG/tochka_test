var tasks = false;
var currentPage = document.location.hash;
currentPage = currentPage.replace('#', '');
if(!currentPage){ currentPage = 1; }
var rowOnPage = 100;

$.getJSON('http://dev2.huskygeek.ru/api/v1/task', function(data){
  tasks = data;

  setPage(currentPage);
  setPagination();
});

function getTask(id){
  $.getJSON('http://dev2.huskygeek.ru/api/v1/task/'+id, function(data){
    tasks[id] = data;
    tasks[id].loaded = true;
    setModal(id);
  });
}

function setRow(id){
  var table = document.getElementById('main-table');

  var elemContainer = document.createElement('tr');
    elemContainer.className = "task-row";
    elemContainer.setAttribute("onClick", "setModal("+ id +");");
  var elemId = document.createElement('td');
  var elemTitle = document.createElement('td');
  var elemDate = document.createElement('td');

  table.appendChild(elemContainer);
  elemContainer.appendChild(elemId);
  elemContainer.appendChild(elemTitle);
  elemContainer.appendChild(elemDate);

  var textElemId = document.createTextNode(tasks[id].id);
  var textElemTitle = document.createTextNode(tasks[id].title);
  var textElemDate = document.createTextNode(tasks[id].date);

  elemId.appendChild(textElemId);
  elemTitle.appendChild(textElemTitle);
  elemDate.appendChild(textElemDate);
}

function setPage(page){
  var startRow = (page-1)*rowOnPage;
  var endRow = page*rowOnPage-1;
  $('.task-row').remove();
  for(i = startRow; i <= endRow; ++i){
    setRow(i);
  }
}

function setModal(id){
  if(tasks[id].loaded){
    var body = document.getElementsByTagName('body')[0];
    var elemContainer = document.createElement('div');
      elemContainer.id = "task-modal"

    var elemId = document.createElement('div');
    var elemTitle = document.createElement('div');
    var elemDate = document.createElement('div');
    var elemAuthor = document.createElement('div');
    var elemStatus = document.createElement('div');
    var elemDescription = document.createElement('div');

    body.appendChild(elemContainer);
    elemContainer.appendChild(elemId);
    elemContainer.appendChild(elemTitle);
    elemContainer.appendChild(elemDate);
    elemContainer.appendChild(elemAuthor);
    elemContainer.appendChild(elemStatus);
    elemContainer.appendChild(elemDescription);

    var textElemId = document.createTextNode("ID: "+tasks[id].id);
    var textElemTitle = document.createTextNode("Описание: "+tasks[id].title);
    var textElemDate = document.createTextNode("Дата: "+tasks[id].date);
    var textElemAuthor = document.createTextNode("Автор: "+tasks[id].author);
    var textElemStatus = document.createTextNode("Статус: "+tasks[id].status);
    var textElemDescription = document.createTextNode("Описание: "+tasks[id].description);

    elemId.appendChild(textElemId);
    elemTitle.appendChild(textElemTitle);
    elemDate.appendChild(textElemDate);
    elemAuthor.appendChild(textElemAuthor);
    elemStatus.appendChild(textElemStatus);
    elemDescription.appendChild(textElemDescription);

  } else {

    getTask(id);

  }

  $('#task-modal').click(function(){
    $('#task-modal').remove();
  });

}

function setPagination(){
  var body = document.getElementsByTagName('body')[0];

  var elemContainer = document.createElement('div');
    elemContainer.id = "paginator";

  var countPage = tasks.length/rowOnPage;

  for(i=0; i<=countPage; ++i){
    var elemPageLink = document.createElement('a');
    elemPageLink.href = "#"+(i+1);
    var textElemPageLink = document.createTextNode(i+1);
    elemPageLink.appendChild(textElemPageLink);
    elemPageLink.setAttribute("onClick", "setPage("+ (i+1) +");");
    elemContainer.appendChild(elemPageLink);
    
  }

  body.appendChild(elemContainer);
}