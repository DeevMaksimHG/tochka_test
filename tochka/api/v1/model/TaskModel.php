<?php 


class TaskModel {

	public $id;
	public $title;
	public $date;
	public $author;
	public $status;
	public $description;

	private static $num_rows = 1000;

	public static function getNumRows(){
		return self::$num_rows;
	}

	public static function findAll(){
		for ($i=0; $i <= self::$num_rows ; $i++) { 
			$result[] = array(
				'id' => $i,
				'title' => 'Задача '.$i,
				'date' => date("F j, Y, G:i", time()+$i*3600),
				'author' => 'Автор '.$i,
				'status' => 'Статус '.$i,
				'description' => 'Описание '.$i,
			);
		}

		return $result;
	}

	public function findById($id){
		if($id <= self::$num_rows){
			$this->id = $id;
			$this->title = 'Задача '.$id;
			$this->date = date("F j, Y, G:i", time()+$id*3600);
			$this->author = 'Автор '.$id;
			$this->status = 'Статус '.$id;
			$this->description = 'Описание '.$id;
		}
	}
}