<?php 

require_once("./model/TaskModel.php");

class TaskController {


	public function index($id = false){
		if($id){
			$taskModel = new TaskModel();
			$taskModel->findById($id);
			if($taskModel->id){
				print_r( json_encode(get_object_vars($taskModel)) );
			} else {
				header('HTTP/1.1 404 Not Found');
      			header('Status: 404 Not Found');
			}
		} else {
			$tasks = TaskModel::findAll();
			print_r( json_encode($tasks) );
		}
	}
}