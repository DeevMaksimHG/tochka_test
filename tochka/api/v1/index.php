<?php

header("Content-type: application/json; charset=utf-8");

$route = explode("/", $_GET['_controller']);
$controllerName = ucfirst($route[0]).'Controller';

require_once("./controller/".$controllerName.'.php');
$controller = new $controllerName;

if(isset($route[1])){
	$controller->index($route[1]);
} else {
	$controller->index();
}